var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = function (grunt) {
    grunt.initConfig({
        clean: ['./build'],
        concurrent: {
            dev: ['nodemon:dev', 'webpack:dev'],
            options: {
                logConcurrentOutput: true
            }
        },
        nodemon: {
            dev: {
                script: './lib/server.js',
                options: {
                    ignore: ['build/**'],
                    ext: 'js,jsx'
                }
            }
        },
        webpack: {
            dev: {
                stats: {
                    colors: true
                },
                devtool: 'source-map',
                watch: true,
                keepalive: true
            },
            prod: {
            },
            options: {
                resolve: {
                    extensions: ['', '.js', '.jsx']
                },
                entry: './lib/client.js',
                output: {
                    path: './build',
                    publicPath: '/public/',
                    filename: '[name].js',
                    chunkFilename: "[id].js"
                },
                module: {
                    loaders: [
                        {test: /\.(png|jpg|ttf|eot|svg|woff|woff2)$/, loader: "file"},
                        {test: /\.(css|less)$/, loader: ExtractTextPlugin.extract("style", "css!autoprefixer!less")},
                        {test: /\.(js|jsx)$/, exclude: /node_modules/, loader: require.resolve('babel-loader')},
                        {test: /\.json$/, loader: "json"}
                    ]
                },
                resolve: {
                    alias: {
                        'bootstrap.less': __dirname + '/node_modules/bootstrap/less/bootstrap.less',
                        'mapbox.css': __dirname + '/node_modules/mapbox.js/theme/style.css'
                    }
                },
                plugins: [
                    // Protects against multiple React installs when npm linking
                    new webpack.NormalModuleReplacementPlugin(/^react?$/, require.resolve('react')),
                    new webpack.NormalModuleReplacementPlugin(/^react(\/addons)?$/, require.resolve('react/addons')),
                    new ExtractTextPlugin("[name].css")
                ]
            }
        }
    });

    grunt.loadNpmTasks('grunt-concurrent');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-nodemon');
    grunt.loadNpmTasks('grunt-webpack');

    grunt.registerTask('default', ['clean', 'webpack:prod']);
    grunt.registerTask('dev', ['clean', 'concurrent:dev']);
};
