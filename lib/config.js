import url from 'url';

var redisUrl = process.env.REDIS_URL || 'redis://127.0.0.1:6379';
var redisUrlParts = url.parse(redisUrl);

var config = {
    mongodb: {
        url: process.env.MONGOLAB_URI || 'mongodb://127.0.0.1:27017/test'
    },
    redis: {
        host: redisUrlParts.hostname,
        port: redisUrlParts.port,
        auth: redisUrlParts.auth && redisUrlParts.auth.split(":")[1] || ''
    },    
    mapbox: {
        accessToken: 'pk.eyJ1IjoiZHVjcmV0MjAxIiwiYSI6ImRjOGQ1ZWFkZmY1N2ZhNWI2NzA4OTY4NzAwMzM4Mjg5In0.fZHshR6dYntloiiTRqbHDg'
    },
    sendgrid: {
    	username: process.env.SENDGRID_USERNAME || '',
    	password: process.env.SENDGRID_PASSWORD || ''
    },
    server: {
        url: process.env.SERVER_URL || 'http://127.0.0.1:3000',
        jwtSecret: process.env.SERVER_JWT_SECRET || 'livepeak'
    }
};

export default config;