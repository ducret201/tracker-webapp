'use strict';

import Promise from 'bluebird';
import React from 'react';
import L from 'leaflet';
import {createElementWithContext} from 'fluxible-addons-react';
import debug from 'debug';
import app from './app';
import config from './config';

// config logger
debug.enable('*');
debug = debug('tracker:client');

// add bluebird support
Promise.promisifyAll(require('fluxible'));
Promise.promisifyAll(require('react'));

// config mapbox
L.mapbox.accessToken = config.mapbox.accessToken;

// for chrome dev tool support
window.React = React;

debug('rehydrating app');
app.rehydrate(window.App).then(function(context) {
    // for chrome dev tool support
    window.context = context;

    debug('react rendering');
    React.render(createElementWithContext(context), document.getElementById('app'), function() {
        debug('react rendered');
    });
});
