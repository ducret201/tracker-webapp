'use strict';

import React from 'react';
import HtmlComponent from './components/html.jsx';
import ErrorComponent from './components/error.jsx';

HtmlComponent = React.createFactory(HtmlComponent);
ErrorComponent = React.createFactory(ErrorComponent);

function errorHandler(err, req, res, next) {
	var status = err.status || err.statusCode || 500;
	var props = {
		status: status,
		message: err.message
	};

	if (status == 500) {
		console.error(err.stack);
	}

	switch (status) {
		case 404:
			Object.assign(props, {message: 'Not found'});
			break;
		case 500:
			Object.assign(props, {message: 'Internal server error'});
			break;			
	}


    var html = React.renderToStaticMarkup(HtmlComponent({
    	static: true,
        markup: React.renderToString(ErrorComponent(props))
    }));

  	res.status(status).send(html);
}

export default errorHandler;