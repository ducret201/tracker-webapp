'use strict';

import React from 'react';
import {provideContext} from 'fluxible-addons-react';
import {handleHistory} from 'fluxible-router';

if (typeof window != 'undefined') {
    // client side only
    require('mapbox.js');
    require('mapbox.css');
    require('../../common/leaflet/animatedMarker');
    require('./app.less');
}

class AppComponent extends React.Component{

    render() {
        var Handler = this.props.currentRoute.get('appHandler');

        return (
            <div>
                <Handler />
            </div>
        );
    }
}

// wrap with history handler
AppComponent = handleHistory(AppComponent);

// wrap with context
AppComponent = provideContext(AppComponent);

export default AppComponent;
