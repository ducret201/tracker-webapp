'use strict';

import React from 'react';

class ErrorComponent extends React.Component{

    render() {
        return (
            <div className='error'>
                <div className='container'>
                    <div className='row'>
                        <div className='col-sm-12'>
                            <h1>{this.props.status}</h1>
                            <p>{this.props.message}</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ErrorComponent;
