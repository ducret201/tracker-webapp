'use strict';

import React from 'react';
import serialize from 'serialize-javascript';

class HtmlComponent extends React.Component {

    browserSupportScript(cssProperty) {

        function script(cssProperty) {
            var errorHtml = '' + 
                '<div class="error">' +
                '   <div class="container">' +
                '       <div class="row">' +
                '           <div class="col-sm-12">' +
                '               <h4>Your browser is out-of-date!</h4>' +
                '               <p>Update your browser to view this website correctly. <a href="http://outdatedbrowser.com/">Update my browser now</a></p>' +
                '           </div>' +
                '       </div>' +
                '   </div>' +
                '</div>';

            function supports(prop) {
                var divElement = document.createElement('div');

                if (prop in divElement.style) return true;

                var vendors = 'Khtml Ms O Moz Webkit'.split(' ');
                var vendorsLength = vendors.length;
                var prop = prop.replace(/^[a-z]/, function(val) {
                    return val.toUpperCase();
                });

                while(vendorsLength--) {
                    if (vendors[vendorsLength] + prop in divElement.style ) return true;
                }
                return false;
            };

            if (!supports(cssProperty)) {
                var element = document.createElement('div');
                element.className = 'toplayer';
                element.innerHTML = errorHtml;
                document.body.appendChild(element);
            }
        }

        if (this.props.browserSupport) return '(' + serialize(script) + ')(\'' + this.props.browserSupport + '\');';
        return '(' + serialize(function script(){}) + ')();'
    }

    stateScript() {

        return '' +
            '(function script() {' +
            '   window.App=' + serialize(this.props.state) + ';' +
            '})();';
    }

    inlineScript() {

        function script() {
            var trackStore = window.App.context.dispatcher.stores.TrackStore;
            if (trackStore) {
                trackStore.clientTimestamp = new Date().getTime();
            }
        };

        return '(' + serialize(script) + ')();'
    }

    render() {
        var markupId = this.props.static && 'static' || 'app';
        var browserSupportScript = this.props.static && (<script></script>) || (<script dangerouslySetInnerHTML={{__html: this.browserSupportScript()}}></script>);
        var stateScript = this.props.static && (<script></script>) || (<script dangerouslySetInnerHTML={{__html: this.stateScript()}}></script>);
        var inlineScript = this.props.static && (<script></script>) || (<script dangerouslySetInnerHTML={{__html: this.inlineScript()}}></script>);
        var mainScript = this.props.static && (<script></script>) || (<script src="/public/main.js" defer></script>);

        return (
            <html>
            <head>
                <title>{this.props.title}</title>
                <meta charSet="utf-8" />
                <meta name="viewport" content="width=device-width, user-scalable=no" />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <link href="/public/main.css" rel="stylesheet" />
                <link href='//fonts.googleapis.com/css?family=Roboto:500,100,300,700,400|Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css' />
            </head>
            <body>
                <div id={markupId} dangerouslySetInnerHTML={{__html: this.props.markup}}></div>
                {browserSupportScript}
                {stateScript}
                {inlineScript}
                {mainScript}
            </body>
            </html>
        );
    }
}

export default HtmlComponent;
