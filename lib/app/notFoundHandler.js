'use strict';

import React from 'react';
import HtmlComponent from './components/html.jsx';
import ErrorComponent from './components/error.jsx';

HtmlComponent = React.createFactory(HtmlComponent);
ErrorComponent = React.createFactory(ErrorComponent);

function notFoundHandler(req, res, next) {
	var status = 404;
	var props = {
		status: status,
		message: 'Not found'
	}

	console.info('Path not found', req.url);

    var html = React.renderToStaticMarkup(HtmlComponent({
    	static: true,    	
        markup: React.renderToString(ErrorComponent(props))
    }));

  	res.status(status).send(html);
}

export default notFoundHandler;