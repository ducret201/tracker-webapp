'use strict';

function apiErrorHandler(err, req, res, next) {
	console.error(err.stack);

	var status = err.status || err.statusCode || 500;
	var json = {
		error: err.name,
		message: err.message
	};

	if (status == 500) {
		console.error(err.stack);
	}

	switch (status) {
		case 404:
			Object.assign(json, {error: 'NotFoundError', message: 'Not found'});
			break;
		case 500:
			Object.assign(json, {error: 'ServerError', message: 'Internal server error'});
			break;			
	}

	switch (err.name) {
		case 'ValidationError':
			Object.assign(json, {validation: err.validation});
			break;
	};

  	res.status(status).send(json);
}

export default apiErrorHandler;