'use strict';

class Distance {

	constructor(value) {
        this.value = value || 0;
    }

    format(unit) {
    	if (unit == 'km') return (Math.round(this.value / 100) / 10) + ' km';
    	if (unit == 'm') return Math.round(this.value) + ' m';

		if (this.value / 1000 >= 1) return this.format('km');
		return this.format('m');
    }
}

function distance(value) {
	return new Distance(value);
}

export {Distance};
export default distance;