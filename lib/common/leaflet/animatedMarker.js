'use strict';

import L from 'leaflet';

class AnimatedMarker extends L.Marker {

    constructor(latLng, options) {
        options = options || {};
        options.duration = options.duration || 1000;

        super(latLng, options);
    }

    setLatLng(latLng) {
        var oldLatLng = this._latlng;
        super.setLatLng(latLng);

        if (!this._latlng.equals(oldLatLng)) {
            if (L.DomUtil.TRANSITION) {
                if (this._icon) {
                    this._icon.style[L.DomUtil.TRANSITION] = 'all ' + this.options.duration + 'ms linear';
                }
                if (this._shadow) {
                    this._shadow.style[L.DomUtil.TRANSITION] = 'all ' + this.options.duration + 'ms linear';
                }

                setTimeout(() => {
                    if (this._icon) {
                        this._icon.style[L.DomUtil.TRANSITION] = null;
                    }
                    if (this._shadow) {
                        this._shadow.style[L.DomUtil.TRANSITION] = null;
                    }

                    this.fire('moveend');
                }, this.options.duration);
            }
        }
    }
}

L.AnimatedMarker = AnimatedMarker;

L.animatedMarker = function (latlngs, options) {
    return new L.AnimatedMarker(latlngs, options);
};