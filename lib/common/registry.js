'use strict';

class Registry {

	get(name, factory) {
		return this[name] || (this[name] = factory());
	}

	list(name) {
		return Object.keys(this)
		.filter((k) => k.startsWith(name + ':'))
		.map((k) => this[k])
	}
}

export {Registry};
export default new Registry();