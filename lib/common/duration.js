'use strict';

class Duration {

	constructor(value) {
        this.value = value && Math.round(value / 1000) || 0; // seconds
    }

    format() {
        var timestamp = this.value;
        var hours = Math.floor(timestamp / 3600);
        timestamp = timestamp % 3600;
        var minutes = Math.floor(timestamp / 60);
        timestamp = timestamp % 60;
        var seconds = timestamp;

        var components = []
        if (hours > 0) components.push(hours + ' h');
        if (minutes > 0) components.push(minutes + ' min');
        if (seconds > 0) components.push(seconds + ' s');

		return components.length > 0 && components.slice(0, 2).join(' ') || '0 s';
    }
}

function duration(value) {
	return new Duration(value);
}

export {Duration};
export default duration;