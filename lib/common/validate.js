'use strict';

import jjv from 'jjv';
import Promise from 'bluebird';

var validator = jjv();

class ValidationError extends Error {

	constructor(message, validation) {
		super(message);
		Error.captureStackTrace(this, this.constructor);
		this.message = message; 
		this.validation = validation;
	}

	get name() {
		return 'ValidationError';
	}

	get statusCode() {
		return '400';
	}
}

function validate(schema, object) {
	return Promise.try(function() {
	    var errors = validator.validate(schema, object, {useDefault: true, removeAdditional: true});
	    if (errors) throw new ValidationError('Validation error', errors.validation);
	    return object;
	});
}

export {ValidationError};
export default validate;