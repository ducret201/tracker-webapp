'use strict';

class NotFoundError extends Error {

	constructor(message) {
		super(message);
		Error.captureStackTrace(this, this.constructor);
		this.message = message; 
	}

	get name() {
		return 'NotFoundError';
	}

	get statusCode() {
		return '404';
	}

	set statusCode(status) {
	}	
}

export {NotFoundError};