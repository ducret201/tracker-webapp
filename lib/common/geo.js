'use strict';

function distanceBetween(latLng1, latLng2) {
    var R = 6371000; // Radius of the earth in m
    var {lat: lat1, lng: lng1} = toLatLng(latLng1);
    var {lat: lat2, lng: lng2} = toLatLng(latLng2);
    var latR = toRad(lat2 - lat1);
    var lngR = toRad(lng2 - lng1);

    var a = Math.sin(latR/2) * Math.sin(latR/2) + Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * Math.sin(lngR/2) * Math.sin(lngR/2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c;
    
    return Math.floor(d);
}

function format(latLng) {
    var {lat, lng} = toLatLng(latLng);
    return lat + ', ' + lng;
}

function toRad(deg) {
  return deg * (Math.PI/180)
}

function toLatLng(a) {
    if (Array.isArray(a)) {
        return {lat: a[0], lng: a[1]};
    }
    return a;
}

export default {
    distanceBetween: distanceBetween,
    format: format
};