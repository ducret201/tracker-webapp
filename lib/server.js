'use strict';

var babelRegister = require('babel/register');
var Promise = require('bluebird');
var http = require('http');
var express = require('express');
var jwt = require('express-jwt');
var favicon = require('serve-favicon');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var csrf = require('csurf');
var debug = require('debug');
var React = require('react');
var navigateAction = require('fluxible-router').navigateAction;
var RouteStore = require('fluxible-router').RouteStore;
var createElementWithContext = require('fluxible-addons-react/createElementWithContext');
var WebSocketServer = require('websocket').server;
var WebSocketRouter = require('websocket').router;
var registry = require('./common/registry').default;
var notFoundHandler = require('./app/notFoundHandler');
var errorHandler = require('./app/errorHandler');
var apiErrorHandler = require('./app/apiErrorHandler');
var config = require('./config');
var app = require('./app');

// config logger
debug.enable('*');
debug = debug('tracker:server');

// add bluebird support
Promise.promisifyAll(require("mongodb"));
Promise.promisifyAll(require("mongodb").MongoClient);

// create REACT root component
var HtmlComponent = require('./app/components/html.jsx');

// register FLUXIBLE services
var fetchrPlugin = app.getPlugin('FetchrPlugin');
fetchrPlugin.registerService(require('./track/service'));

// config web server
var server = express();
server.set('state namespace', 'App');
// register static middlewares
server.use(favicon(__dirname + '/../favicon.ico'));
server.use('/public', express.static(__dirname + '/../build'));
// register dynamic middlewares
server.use(cookieParser());
server.use(bodyParser.json());
// register api routes
server.use('/api/v1', jwt({secret: config.server.jwtSecret}));
server.use('/api/v1/track', require('./track/api'));
server.use('/api/v1', apiErrorHandler);
// register default
server.use(csrf({cookie: true}));
server.use(fetchrPlugin.getXhrPath(), fetchrPlugin.getMiddleware());
server.use(function (req, res, next) {
    var context = app.createContext({
        req: req,
        xhrContext: {
            _csrf: req.csrfToken()
        }
    });

    debug('executing action');
    context.executeAction(navigateAction, {url: req.url, type: 'pageload'}).then(function() {
        debug('react rendering');

        var state = app.dehydrate(context);
        var markup = React.renderToString(createElementWithContext(context));
        var currentRoute = context.getStore(RouteStore).getCurrentRoute();

        var html = React.renderToStaticMarkup(React.createElement(HtmlComponent, {
            state: state,
            markup: markup,
            browserSupport: currentRoute.get('browserSupport')
        }));

        debug('sending response');
        res.send('<!DOCTYPE html>' + html);
    }).catch(next);
});
server.use(notFoundHandler);
server.use(errorHandler);

// config web socket server
var httpServer = http.createServer(server);
var wsServer = new WebSocketServer({httpServer: httpServer, autoAcceptConnections: false});
// register web socket routes
var wsRouter = new WebSocketRouter();
wsRouter.mount('/socket/track', 'track-protocol', require('./track/socket'));
wsRouter.attachServer(wsServer);

// start server
var port = process.env.PORT || 3000;
httpServer.listen(port, function(err) {
    if (err) {
        debug(err)
        return
    }

    debug('listening on port ' + port);
});

process.on('SIGTERM', function() {
    // stop server
    debug('shutdown');

    var quitRedisPromises = registry.list('redis').map(function(client) {
        return client.quit();
    });
    var closeQueuePromises = registry.list('queue').map(function(queue) {
        return queue.close();
    });

    var promises = []
    promises.push.apply(promises, quitRedisPromises);
    promises.push.apply(promises, closeQueuePromises);

    Promise.all(promises).then(function() {
        process.exit(0);
    })
});