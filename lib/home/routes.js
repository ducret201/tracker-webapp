'use strict';

var routes = {
    home: {
        path: '',
        method: 'get',
        browserSupport: 'boxShadow', // IE9 and up
        appHandler: require('./components/home.jsx')
    }
};

export default routes;