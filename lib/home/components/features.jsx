'use strict';

import React from 'react';

class FeaturesComponent extends React.Component {

    render() {
        return (
            <div className='home-section home-features'>
            	<div className='container'>
	            	<div className='row'>
		            	<div className='col-sm-4'>
		            		<span className='glyphicon glyphicon-stack'>
		            			<i className='glyphicon glyphicon-stack-2x glyphicon-circle red'></i>
		            			<i className='glyphicon glyphicon-stack-1x glyphicon-lock'></i>
		            		</span>
		            		<h3>Full privacy control</h3>
		            		<p>Track your activities privately and share them only with your friends and family at home.</p>
		            	</div>
		            	<div className='col-sm-4'>
		            		<span className='glyphicon glyphicon-stack'>
		            			<i className='glyphicon glyphicon-stack-2x glyphicon-circle blue'></i>
		            			<i className='glyphicon glyphicon-stack-1x glyphicon-user'></i>
		            		</span>
		            		<h3>Multiple tracking profiles</h3>
		            		<p>Whether you are on the way back home or down the ski runs, just pick the right profile and start live tracking in no time.</p>
		            	</div>
		            	<div className='col-sm-4'>
		            		<span className='glyphicon glyphicon-stack'>
		            			<i className='glyphicon glyphicon-stack-2x glyphicon-circle yellow'></i>
		            			<i className='glyphicon glyphicon-stack-1x glyphicon-flash'></i>
		            		</span>
		            		<h3>Minimal battery consumption</h3>
		            		<p>Track with high accuracy when moving fast or save battery life during your day long activities. The choice is yours.</p>
		            	</div>
	            	</div>
	            	<div className='row'>
		            	<div className='col-sm-12'>
            				<h2>Get it now! Download Livepeak for your Android device.</h2>
            				<div className='stores'>
	            				<a href="https://play.google.com/store/apps/details?id=com.livepeak.mobile">
									<img alt="Android app on Google Play" src="//play.google.com/intl/en_us/badges/images/apps/en-play-badge.png" />
								</a>
							</div>
		            	</div>
	            	</div>
            	</div>
            </div>
        );
    }
}

export default FeaturesComponent;