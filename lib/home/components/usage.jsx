'use strict';

import React from 'react';

class UsageComponent extends React.Component {

    render() {
        return (
            <div className='home-section home-usage'>
            	<div className='container'>
	            	<div className='row'>
		            	<div className='col-sm-6 col-md-offset-1 col-md-4'>
		            		<span className='glyphicon glyphicon-stack'>
		            			<i className='glyphicon glyphicon-stack-2x glyphicon-circle yellow'></i>
		            			<i className='glyphicon glyphicon-stack-1x glyphicon-map-marker'></i>
		            		</span>
		            		<h2>Let your fans follow your steps</h2>
		            		<p>Share your position in real time during your workouts and bring a new experience to your fans, wherever you are.</p>
		            	</div>
		            	<div className='col-sm-6 col-md-offset-2 col-md-4'>
		            		<span className='glyphicon glyphicon-stack'>
		            			<i className='glyphicon glyphicon-stack-2x glyphicon-circle red'></i>
		            			<i className='glyphicon glyphicon-stack-1x glyphicon-heart'></i>
		            		</span>
		            		<h2>Give your loved ones peace of mind</h2>
		            		<p>Let your family know where you are whether you are going back home late at night or on the mountain trail.</p>
		            	</div>
	            	</div>
            	</div>            
            </div>
        );
    }
}

export default UsageComponent;