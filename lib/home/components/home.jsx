'use strict';

import React from 'react';
import Pitch from './pitch.jsx';
import Usage from './usage.jsx';
import Sample from './sample.jsx';
import Features from './features.jsx';

class HomeComponent extends React.Component {

    render() {
        return (
            <div className='home'>
                <div className='home-header'>
                    <div className='container'>
                        <div className='row'>
                            <div className='col-sm-12'>
                                <i className='logo'></i> 
                                <h3>Livepeak <small>Beta</small></h3>
                            </div>
                        </div>
                    </div>
                </div>
                <Pitch />
                <Usage />
                <Sample />
                <Features />
            </div>
        );
    }
}

export default HomeComponent;