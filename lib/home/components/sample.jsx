'use strict';

import React from 'react';

class SampleComponent extends React.Component {

    render() {
        return (
            <div className='home-section home-sample'>
            	<div className='container'>
	            	<div className='row'>
		            	<div className='hidden-xs col-sm-6'>
							<div className="section" />
		            	</div>
		            	<div className='col-xs-12 col-sm-6'>
            				<div className="phone" />		            	
		            	</div>
	            	</div>
            	</div>
            </div>
        );
    }
}

export default SampleComponent;