'use strict';

import React from 'react';

class PitchComponent extends React.Component {

    render() {
        return (
            <div className='home-section home-pitch'>
            	<div className="activities" />
            	<div className='container'>
	            	<div className='row'>
		            	<div className='col-sm-5'>
		            		<h1>
		            			<div>Live tracking</div>
		            			<div>for your outdoor</div>
		            			<div>activities</div>		            			
		            		</h1>
		            		<p>Livepeak uses your phone's GPS to share your location in real time</p>
		            	</div>
	            	</div>
            	</div>
            </div>
        );
    }
}

export default PitchComponent;