'use strict';

var routes = {
    track: {
        path: '/:id',
        method: 'get',
        browserSupport: 'transition', // IE10 and up
        appHandler: require('./components/trackMap.jsx'),
        action: require('./actions/openTrack')
    }
};

export default routes;