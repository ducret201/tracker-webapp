'use strict';

import Promise from 'bluebird';
import {MongoClient} from 'mongodb';
import bull from 'bull';
import Sendgrid from 'sendgrid';
import debug from 'debug';
import {NotFoundError} from '../../common/errors';
import config from '../../config'

debug = debug('tracker:track:shareTrack');

var SUBJECT_1_TEMPLATE = '{{userName}} started a new live track';

var SUBJECT_2_TEMPLATE = 'Check out a new live track';

var MESSAGE_TEXT_1_TEMPLATE = '{{userName}} started a new live track: {{name}}\r\n'
	+ '{{trackUrl}}';

var MESSAGE_TEXT_2_TEMPLATE = 'Check out a new live track: {{name}}\r\n'
	+ '{{trackUrl}}';

var MESSAGE_HTML_1_TEMPLATE = '<p>{{userName}} started a new live track: <a href="{{trackUrl}}">{{name}}</a></p>'
	+ '<p><a href="{{trackUrl}}">{{trackUrl}}</a></p>';

var MESSAGE_HTML_2_TEMPLATE = '<p>Check out a new live track: <a href="{{trackUrl}}">{{name}}</a></p>'
	+ '<p><a href="{{trackUrl}}">{{trackUrl}}</a></p>';

var sendgrid = Promise.promisifyAll(new Sendgrid(config.sendgrid.username, config.sendgrid.password));
var queue = bull('track:share', config.redis.port, config.redis.host, {auth_pass: config.redis.auth});

function buildEmail(track) {
	var userName = track.properties.userName;
	var name = track.properties.name;
	var sharing = track.properties.sharing;

	var subject = userName && SUBJECT_1_TEMPLATE || SUBJECT_2_TEMPLATE;
	subject = subject.replace('{{userName}}', userName);

	var textMessage = userName && MESSAGE_TEXT_1_TEMPLATE || MESSAGE_TEXT_2_TEMPLATE;
	textMessage = textMessage.replace('{{userName}}', userName);
	textMessage = textMessage.replace('{{name}}', name);
	textMessage = textMessage.replace('{{trackUrl}}', config.server.url + '/' + track._id);

	var htmlMessage = userName && MESSAGE_HTML_1_TEMPLATE || MESSAGE_HTML_2_TEMPLATE;
	htmlMessage = htmlMessage.replace('{{userName}}', userName);
	htmlMessage = htmlMessage.replace('{{name}}', name);
	htmlMessage = htmlMessage.replace(/{{trackUrl}}/g, config.server.url + '/' + track._id);

	var email = new sendgrid.Email({
		from: 'notification@golivepeak.com',
		fromname: 'Livepeak',
		subject: subject,
		text: textMessage,
		html: htmlMessage
	});

	sharing.email.recipients.forEach((recipient) => email.addTo(recipient));

	return email;
}

queue.process(function(job) {
	var trackId = job.data.trackId

	return MongoClient.connectAsync(config.mongodb.url).bind({}).then(function(db) {
		this.db = db;
		var query = {_id: trackId, _status: {$ne: 'EMAIL_SENT'}, 'properties.sharing.email': {$exists: true}};
		return db.collection('tracks').find(query).toArrayAsync();
	}).then(function(response) {
		this.response = response;
		if (!response[0]) throw new NotFoundError('No pending email');
	}).then(function() {
		return sendgrid.sendAsync(buildEmail(this.response[0]));
	}).then(function() {	
		var query = {_id: trackId, _status: {$ne: 'EMAIL_SENT'}};
		var update = {$addToSet: {_status: 'EMAIL_SENT'}};
		return this.db.collection('tracks').updateAsync(query, update);
	}).then(function() {
		debug('email sent for track ', trackId);
	}).catch(NotFoundError, function() {
		debug('no pending email for track ', trackId);
	});
});

export default queue;
