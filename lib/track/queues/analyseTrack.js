'use strict';

import {MongoClient} from 'mongodb';
import bull from 'bull';
import debug from 'debug';
import geo from '../../common/geo'
import config from '../../config'

debug = debug('tracker:track:analyseTrack');

var queue = bull('track:analyse', config.redis.port, config.redis.host, {auth_pass: config.redis.auth});

function computeTrackDuration(track) {
    var startTimeStamp = track.geometry.coordinates[0][3];
    var endTimeStamp = track.geometry.coordinates[track.geometry.coordinates.length - 1][3];

    return endTimeStamp - startTimeStamp;
}

function computeTrackDistance(track) {
    var startLatLng = track.geometry.coordinates[0];
    var endLatLng = track.geometry.coordinates[track.geometry.coordinates.length - 1];

    return geo.distanceBetween(startLatLng, endLatLng);
}

queue.process(function(job) {
	var trackId = job.data.trackId

	return MongoClient.connectAsync(config.mongodb.url).bind({}).then(function(db) {
		this.db = db;
		return db.collection('tracks').find({_id: trackId}).toArrayAsync();
	}).then(function(response) {
		return this.db.collection('tracks').updateAsync({_id: trackId}, {$set: {
			'properties.duration': computeTrackDuration(response[0]),
			'properties.distance': computeTrackDistance(response[0])}
		});
	}).then(function() {
		debug('track analysed ', trackId);
	});
});

export default queue;
