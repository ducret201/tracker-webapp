'use strict';

function receivePosition(context, payload, done) {
    context.dispatch('TRACK_POSITION_RECEIVED', payload);
    done();
}

export default receivePosition;