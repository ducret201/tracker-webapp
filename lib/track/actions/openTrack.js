'use strict';

function openTrack(context, payload, done) {
    var trackId = payload.get('params').get('id');

    context.service.read('track', {id: trackId}, function(err, track) {
    	if (err) return done(err);

	    context.dispatch('TRACK_OPENED', track);
		done();
    });
}

export default openTrack;