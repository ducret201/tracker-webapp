'use strict';

class Track {

    constructor(data) {
        Object.assign(this, data);
    }

    computeState() {
        if (this.properties.flag == 'Stop') {
            this.properties.state = 'Terminated';
        } else {
            this.properties.state = this.properties.state || 'Running';            
        }
    }

    computeGeometrySize() {
        this.geometry.size = this.geometry.coordinates.length;
    }
}

Track.schema = {
    type: 'object',
    properties: {
        type: {type: 'string', enum: ['Feature']},
        properties: {
            type: 'object',
            properties: {
                name: {type: 'string'},
                userName: {type: 'string'},
                activity: {type: 'string'},
                frequency: {type: 'number'},
                sharing: {
                    type: 'object',
                    properties: {
                        message: {type: 'string'},
                        email: {
                            type: 'object',
                            properties: {
                                recipients: {type: 'array'}
                            },
                            required: ['recipients']
                        }
                    },
                    required: ['message']
                },
                flag: {type: 'string', enum: ['Stop']}
            },
            required: ['name', 'activity', 'frequency']
        },
        geometry: {
            type: 'object',
            properties: {
                type: {type: 'string', enum: ['LineString']},
                coordinates: {type: 'array'}
            },
            required: ['type', 'coordinates']
        }
   },
   required: ['type', 'properties', 'geometry']
};

class Position {

    constructor(data) {
        Object.assign(this, data);
    }

    truncateCoordinates(beginCoordinate) {
        if (!beginCoordinate) return this.geometry.coordinates;
        return this.geometry.coordinates = this.geometry.coordinates.filter((coordinate) => coordinate[3] > beginCoordinate[3]);
    }
}

Position.schema = {
    type: 'object',
    properties: {
        type: {type: 'string', enum: ['Feature']},
        properties: {
            type: 'object',
            properties: {
                flag: {type: 'string', enum: ['Stop']}
            }
        },
        geometry: {
            type: 'object',
            properties: {
                type: {type: 'string', enum: ['MultiPoint']},
                coordinates: {type: 'array'}
            },
            required: ['type', 'coordinates']
        }
   },
   required: ['type', 'properties', 'geometry']
};


export {Track, Position};