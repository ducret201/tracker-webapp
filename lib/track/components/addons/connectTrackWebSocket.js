import React from 'react'
import {connectToStores} from 'fluxible-addons-react';
import debug from 'debug';
import receivePosition from '../../actions/receivePosition';
import TrackStore from '../../store';

debug = debug('tracker:track:WebSocketConnection');

function wsUri(path) {
    var wsProtocol = (window.location.protocol == 'https:') && 'wss:' || 'ws:';
    var wsHost = window.location.host;
    return wsProtocol + '//' + wsHost + path;
}

function connectTrackWebSocket(Component) {

    class WebSocketConnection extends React.Component {

        componentDidMount() {
            this._open();
        }

        componentWillUnmount() {
            window.clearTimeout(this._openTimer);            
            this._close();
        }

        _open() {
            if (this._webSocket && (this._webSocket.readyState == WebSocket.CONNECTING || this._webSocket.readyState == WebSocket.OPEN)) return;

            var self = this;
            this._webSocket = new WebSocket(wsUri('/socket/track'), 'track-protocol');

            this._webSocket.onmessage = function(event) {
                var data = JSON.parse(event.data);
                debug('message received', data);

                self.context.executeAction(receivePosition, data);
            };

            this._webSocket.onopen = function () {
                debug('webSocket opened');

                this.send(JSON.stringify({
                    type: 'Action',
                    action: 'Subscribe',
                    channel: 'track:' + self.props.trackId + ':position'
                }));
            };

            this._webSocket.onclose = function (error) {
                debug('webSocket closed', error);

                if (error) {
                    // schedule reconnection
                    debug('attempt to reconnect');
                    window.clearTimeout(self._openTimer);
                    this._openTimer = window.setTimeout(self._open.bind(self), 5000);                    
                }
            };            
        }

        _close() {
            if (!this._webSocket || this._webSocket.readyState == WebSocket.CLOSING || this._webSocket.readyState == WebSocket.CLOSED) return;
            this._webSocket.close();            
        }

        render() {
            return <Component {...this.props} {...this.state} />;
        }
    }

    WebSocketConnection.contextTypes = {
        executeAction: React.PropTypes.func.isRequired
    };

    WebSocketConnection = connectToStores(WebSocketConnection, [TrackStore], function (context, props) {
        var trackStore = context.getStore(TrackStore);

        return {
            trackId: trackStore.track && trackStore.track._id
        };
    });

    return WebSocketConnection;
};

export default connectTrackWebSocket;