'use strict';

import React from 'react';
import {connectToStores} from 'fluxible-addons-react';
import moment from 'moment';
import distance from '../../common/distance'
import duration from '../../common/duration'
import geo from '../../common/geo'
import TrackStore from '../store';

class TrackSectionComponent extends React.Component {

    constructor() {
        this.state = {
            out: false,
            animationDone: true,
            tickToken: 10
        };
    }

    componentWillUnmount() {
        window.clearTimeout(this._tickTimer);
        window.clearTimeout(this._animationTimer);
    }

    componentDidUpdate() {
        // schedule tic to make sure this component is refreshed every minute or more
        window.clearTimeout(this._tickTimer);
        this._tickTimer = window.setTimeout(this._tick.bind(this), 60000);
    }

    _toggle() {
        this.setState({out: !this.state.out, animationDone: false});

        // schedule animation done
        window.clearTimeout(this._animationTimer);
        this._animationTimer = window.setTimeout(this._animationDone.bind(this), 400);
    }

    _tick() {
        this.setState({tickToken: -this.state.tickToken})
    }

    _animationDone() {
        this.setState({animationDone: true})
    }

    render() {
        var activityClass = ' activity-' + this.props.trackProperties.activity.replace(/(.)([A-Z])/, "$1-$2").toLowerCase();
        activityClass += !this.state.out && ' activity-state-' + this.props.trackProperties.state.toLowerCase() || '';
        var trackSectionProfileClass = ' track-section-profile-' + this.props.trackProperties.state.toLowerCase();
        var glyphiconToggleClass = this.state.out && ' glyphicon-menu-up' || ' glyphicon-menu-down';
        var inOutClass = this.state.out && ' out' || ' in';
        inOutClass += this.state.animationDone && ' done' || '';

        var status;
        var timestamp = this.props.positionCoordinates[3] + this.props.gpsTimeDrift;
        switch(this.props.trackProperties.state) {
            case 'Running':
                status = 'Last position received ' + moment(timestamp, 'x').fromNow();
                break;
            case 'Terminated':
                status = 'Track ended ' + moment(timestamp, 'x').fromNow();
                break;
            case 'Lost':
                status = 'Track lost ' + moment(timestamp, 'x').fromNow();            
                break;                
        };

        var UserNameElement;
        if (this.props.trackProperties.userName) {
            UserNameElement = (<p className='user-name'>By {this.props.trackProperties.userName}</p>);
        }

        return (
            <div className='track-section'>
                <div className={'track-section-body track-section-profile' + trackSectionProfileClass + inOutClass}>
                    <span className={'activity' + activityClass}></span>
                    <div className="content">
                        <p className='name'>{this.props.trackProperties.name}</p>
                        {UserNameElement}
                    </div>
                    <a  className='toggle' onClick={this._toggle.bind(this)}><span className={'glyphicon' + glyphiconToggleClass}></span></a>
                </div>
                <div className={'track-section-body track-section-track' + inOutClass}>
                    <p className='status'>{status}</p>
                    <hr></hr>
                    <ul className='metrics'>
                        <li><span className='glyphicon glyphicon-flag'></span><span className='value'>{distance(this.props.trackProperties.distance).format()}</span></li>
                        <li><span className='glyphicon glyphicon-time'></span><span className='value'>{duration(this.props.trackProperties.duration).format()}</span></li>
                        <li><span className='glyphicon glyphicon-plane'></span><span className='value'>{distance(this.props.positionCoordinates[2]).format('m')}</span></li>
                        <li><span className='glyphicon glyphicon-globe'></span><span className='value'>{geo.format(this.props.positionCoordinates)}</span></li>
                    </ul>
                </div>
            </div>
        );
    }
}

TrackSectionComponent = connectToStores(TrackSectionComponent, [TrackStore], function(context, props) {
    var trackStore = context.getStore(TrackStore);

    return {
        trackProperties: trackStore.track && trackStore.track.properties,
        positionCoordinates: trackStore.position && trackStore.position.geometry.coordinates,
        gpsTimeDrift: trackStore.gpsTimeDrift
    };
});

export default TrackSectionComponent;