'use strict';

import React from 'react';
import {connectToStores} from 'fluxible-addons-react';
import TrackStore from '../store';

if (typeof window != 'undefined') {
    // client side only
    var L = require('leaflet');
}

class TrackLineComponent extends React.Component {

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.token == this.props.token) return false;
        return true;
    }

    componentDidUpdate() {
        if (!this.polyline) {
            this.polyline = L.polyline(this.props.trackCoordinates, {
                color: '#4183f0',
                weight: '3',
                opacity: 1
            });
            this.props.map.addLayer(this.polyline);
        } else {
            var newCoordinates = this.props.trackCoordinates.slice(this.polyline.getLatLngs().length);
            newCoordinates.forEach((latLng) => this.polyline.addLatLng(latLng));
        }
    }

    componentWillUnmount() {
        this.props.map.removeLayer(this.polyline);
    }

    render() {
        return null;
    }
}

TrackLineComponent = connectToStores(TrackLineComponent, [TrackStore], function (context, props) {
    var trackStore = context.getStore(TrackStore);

    return {
        trackCoordinates: trackStore.track && trackStore.track.geometry.coordinates
    };
});

export default TrackLineComponent;