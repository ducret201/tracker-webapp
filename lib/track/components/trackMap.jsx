'use strict';

import React from 'react';
import TrackLine from './trackLine.jsx';
import PositionMarker from './positionMarker.jsx';
import TrackSection from './trackSection.jsx';

if (typeof window != 'undefined') {
    // client side only
    var L = require('leaflet');
}

class TrackMapComponent extends React.Component {

    constructor() {
        this.state = {
            trackLineToken: 10
        };
    }

    componentDidMount() {
        var map = L.mapbox.map(React.findDOMNode(this.refs.map), 'ducret201.85521b76', { zoomControl: false });
        map.setView([40.726564, 32.656641], 14);
        map.addControl(new L.Control.Zoom({position: 'topright'}));

        this.setState({
            map: map,
            trackLineToken: -this.state.trackLineToken
        });
    }

    componentWillUnmount() {
    }

    _positionMarkerMoved() {
        this.setState({trackLineToken: -this.state.trackLineToken});
    }

    render() {
        return (
            <div className='track-map'>
                <div ref='map' />
                <div className='track-account'><h3><a href='/'>Livepeak <small>Beta</small></a></h3></div>
                <PositionMarker map={this.state.map} onMove={this._positionMarkerMoved.bind(this)} />
                <TrackLine map={this.state.map} token={this.state.trackLineToken} />
                <TrackSection />
            </div>
        );
    }
}

export default TrackMapComponent;