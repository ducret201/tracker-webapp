'use strict';

import React from 'react';
import {connectToStores} from 'fluxible-addons-react';
import TrackStore from '../store';
import connectTrackWebSocket from './addons/connectTrackWebSocket';

if (typeof window != 'undefined') {
    // client side only
    var L = require('leaflet');
    var markerUrl = require('../resources/img/positionMarker.png');
    console.log(markerUrl);
}

class PositionMarkerComponent extends React.Component {

    componentDidUpdate() {
        if (!this.marker) {
            var icon = L.icon({
                iconUrl: markerUrl,
                iconSize: [24, 24]
            });

            this.marker = L.animatedMarker(this.props.positionCoordinates, {icon: icon});
            this.marker.on('moveend', this.props.onMove);
            this.props.map.addLayer(this.marker);
            this.props.map.setView(this.props.positionCoordinates, 14);
        } else {
            this.marker.setLatLng(this.props.positionCoordinates);
        }
    }

    componentWillUnmount() {
        this.marker.off();
        this.props.map.removeLayer(this.marker);
    }

    render() {
        return null;
    }
}

PositionMarkerComponent = connectToStores(PositionMarkerComponent, [TrackStore], function (context, props) {
    var trackStore = context.getStore(TrackStore);

    return {
        positionCoordinates: trackStore.position && trackStore.position.geometry.coordinates
    };
});

PositionMarkerComponent = connectTrackWebSocket(PositionMarkerComponent);

export default PositionMarkerComponent;