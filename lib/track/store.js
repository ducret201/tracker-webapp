'use strict';

import BaseStore from 'fluxible/addons/BaseStore';
import debug from 'debug';
import geo from '../common/geo';

debug = debug('tracker:track:store');

var REHYDRATE_DELAY = 200;

class TrackStore extends BaseStore {

    constructor() {
        this._gpsTimeDrift = 0;
    }

    _trackOpened(track) {
        this._track = track;
        this._track.properties.state = this._computeTrackState(this._track);
        
        this._position = {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": track.geometry.coordinates[track.geometry.coordinates.length - 1]
            }
        };

        this.emitChange();
    }

    _positionReceived(position) {
        this._track.geometry.coordinates.push(...position.geometry.coordinates);
        this._track.properties.duration = this._computeTrackDuration(this._track);
        this._track.properties.distance = this._computeTrackDistance(this._track);
        this._track.properties.state = this._computeTrackState(this._track, position.properties.flag);

        this._position = {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": position.geometry.coordinates[position.geometry.coordinates.length - 1]
            }
        };

        // schedule tic to handle computation if no position is received
        window.clearInterval(this._tickTimer);
        this._tickTimer = window.setInterval(this._tick.bind(this), this._getTickDelay(this._track));

        this.emitChange();
    }

    _tick() {
        this._track.properties.state = this._computeTrackState(this._track);
        this.emitChange();
    }

    _computeTrackDuration(track) {
        var beginTimestamp = track.geometry.coordinates[0][3];
        var endTimestamp = track.geometry.coordinates[track.geometry.coordinates.length - 1][3];

        return endTimestamp - beginTimestamp;
    }

    _computeTrackDistance(track) {
        var beginLatLng = track.geometry.coordinates[0];
        var endLatLng = track.geometry.coordinates[track.geometry.coordinates.length - 1];

        return geo.distanceBetween(beginLatLng, endLatLng);
    }

    _computeTrackState(track, flag) {
        var state = track.properties.state;

        if (state != 'Terminated' && flag == 'Stop') {
            return 'Terminated';
        }

        if (state == 'Running' || state == 'Lost') {
            var frequency = track.properties.frequency;
            var lastTimestamp = track.geometry.coordinates[track.geometry.coordinates.length - 1][3] + this._gpsTimeDrift;
            var delay = Math.round((new Date().getTime() - lastTimestamp) / 1000); // seconds

            return (delay > this._getToleranceFrequency(frequency)) && 'Lost' || 'Running';
        }

        return state;
    }

    _getTickDelay(track) {
        var frequency = track.properties.frequency;
        return this._getToleranceFrequency(frequency) * 1000;
    }

    _getToleranceFrequency(frequency) {
        if (frequency < 10) return frequency * 2;
        if (frequency < 60) return Math.round(frequency * 1.5);
        return Math.round(frequency * 1.1);
    }

    get track() {
        return this._track;
    }

    get position() {
        return this._position;
    }

    get gpsTimeDrift() {
        return this._gpsTimeDrift;
    }

    dehydrate() {
        return {
            track: this._track,
            position: this._position,
            gpsTimestamp: new Date().getTime()
        };
    }

    rehydrate(state) {
        this._track = state.track;
        this._position = state.position;
        this._gpsTimeDrift = Math.round((state.clientTimestamp - state.gpsTimestamp - REHYDRATE_DELAY) / 1000) * 1000;

        debug('gps time drift', this._gpsTimeDrift);

        // schedule tic to handle computation if no position is received
        window.clearInterval(this._tickTimer);
        this._tickTimer = window.setInterval(this._tick.bind(this), this._getTickDelay(this._track));
    }
};

TrackStore.storeName = 'TrackStore';
TrackStore.handlers = {
    'TRACK_OPENED': '_trackOpened',
    'TRACK_POSITION_RECEIVED': '_positionReceived'
};

export default TrackStore;
