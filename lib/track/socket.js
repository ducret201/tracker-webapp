'use strict';

import redis from 'redis';
import debug from 'debug';
import registry from '../common/registry';
import config from '../config'

debug = debug('tracker:track:socket');

var redisSub = registry.get('redis:sub', () => redis.createClient(config.redis.port, config.redis.host, {auth_pass: config.redis.auth}));

var clientsByChannel = {};

function accept(request) {
    var connection = request.accept(request.origin);
    debug('client ' + connection.remoteAddress + ' connected');

    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            var data = JSON.parse(message.utf8Data);
            debug('message received', data);

            if (data.type == 'Action' && data.action == 'Subscribe') {
                subscribe(data.channel, this);
            } else if (data.type == 'Action' && data.action == 'Unsubscribe') {
                unsubscribe(data.channel, this);
            }
        }
    });

    connection.on('close', function() {
        debug('client ' + this.remoteAddress + ' disconnected');
        unsubscribe(this);
    });
}

function subscribe(channel, client) {
    var clients = clientsByChannel[channel] || (clientsByChannel[channel] = []);
    var index = clients.indexOf(client);
    if (clients.length == 0) {
        redisSub.subscribe(channel);
        debug('subscribed to redis channel ' + channel);
    }
    if (index == -1) {
        clients.push(client);
        debug('client ' + client.remoteAddress + ' subscribed to channel ' + channel);
    }
}

function unsubscribe(channel, client) {
    if (arguments.length == 1) {
        client = channel;
        channel = null;
    }

    if (channel) {
        var clients = clientsByChannel[channel] || [];
        var index = clients.indexOf(client);
        if (index >= 0) {
            clients.splice(index, 1);
            debug('client ' + client.remoteAddress + ' unsubscribed from channel ' + channel);            
        }
        if (clients.length == 0) {
            delete clientsByChannel[channel];
            redisSub.unsubscribe(channel);
            debug('unsubscribed from redis channel ' + channel);
        }
    } else {
        var channels = Object.keys(clientsByChannel);
        channels.forEach((channel) => unsubscribe(channel, client));
    }
}

redisSub.on('message', function(channel, message) {
    var clients = clientsByChannel[channel] || [];
    debug('sending message to ' + clients.length + ' clients', message);

    clients.forEach((client) => client.sendUTF(message));
});


export default accept;