'use strict';

import {Router} from 'express';
import {MongoClient, MongoError} from 'mongodb';
import redis from 'redis';
import bull from 'bull';
import debug from 'debug';
import validate from '../common/validate';
import {NotFoundError} from '../common/errors';
import registry from '../common/registry';
import config from '../config'
import {Track, Position} from './models';

debug = debug('tracker:track:api');

var router = Router();
var redisPub = registry.get('redis:pub', () => redis.createClient(config.redis.port, config.redis.host, {auth_pass: config.redis.auth}));

var analyseQueue = registry.get('queue:analyse', () => bull('track:analyse', config.redis.port, config.redis.host, {auth_pass: config.redis.auth}));
var shareQueue = registry.get('queue:share', () => bull('track:share', config.redis.port, config.redis.host, {auth_pass: config.redis.auth}));

var jobOptions = {
    attempts: 10,
    backoff: 10000
}

router.put('/:id', function(req, res, next) {
    var trackId = req.params.id;

    validate(Track.schema, new Track(req.body)).bind({}).then(function(track) {
        this.track = track;
        this.track._id = trackId;
        this.track.computeState();
        this.track.computeGeometrySize();
        return MongoClient.connectAsync(config.mongodb.url);
    }).then(function(db) {
        var query = {_id: trackId, 'properties.state': {$ne: 'Terminated'}};
        return db.collection('tracks').updateAsync(query, this.track, {upsert: true});
    }).then(function(response) {
        this.response = response;
        if (response.result.upserted) return shareQueue.add({trackId: trackId}, jobOptions);
    }).then(function() {
        debug(this.response.result.upserted && 'new track created' || 'track updated', trackId);
        res.status(this.response.result.upserted && 201 || 200).send();
    }).catch(MongoError, function(error) {
        if (error.code == 11000) throw new NotFoundError('track is ended'); // track is terminated
        throw error;
    }).catch(next);
});

router.post('/:id/position', function(req, res, next) {
    var trackId = req.params.id;

    validate(Position.schema, new Position(req.body)).bind({}).then(function(position) {
        this.position = position;
        return MongoClient.connectAsync(config.mongodb.url);    
    }).then(function(db) {
        this.db = db;
        var query = {_id: trackId, 'properties.state': {$ne: 'Terminated'}};
        var projection = {'geometry.size': 1, 'geometry.coordinates': {$slice: -1}};
        return db.collection('tracks').find(query, projection).toArrayAsync();
    }).then(function(response) {
        this.response = response;
        if (!response[0]) throw new NotFoundError('track doesn\'t exist or is ended');
    }).then(function() {
        var coordinates = this.position.truncateCoordinates(this.response[0].geometry.coordinates[0]);
        var query = {_id: trackId, 'properties.state': {$ne: 'Terminated'}, 'geometry.size': this.response[0].geometry.size};
        var updateGeometry = {$inc: {'geometry.size': coordinates.length}, $push: {'geometry.coordinates': {$each: coordinates}}};
        var updateState = this.position.properties.flag == 'Stop' && {$set: {'properties.state': 'Terminated'}} || {};
        return this.db.collection('tracks').updateAsync(query, Object.assign({}, updateGeometry, updateState));
    }).then(function(response) {
        this.response = response;
        if (response.result.nModified > 0) return analyseQueue.add({trackId: trackId}, jobOptions);
    }).then(function() {
        if (this.response.result.nModified > 0) {
            debug('new positions saved for track', trackId);
            redisPub.publish('track:' + trackId + ':position', JSON.stringify(this.position));
        }
        res.send();
    }).catch(next);
});

export default router;