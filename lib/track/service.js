'use strict';

import {MongoClient} from 'mongodb';
import {NotFoundError} from '../common/errors';
import config from '../config';

var TrackService = {
    name: 'track',
    read: function(req, resource, params, conf, callback) {
        var trackId = params.id;

        MongoClient.connectAsync(config.mongodb.url).bind({}).then(function(db) {
            return db.collection('tracks').find({_id: trackId}).toArrayAsync();
        }).then(function(response) {
	        this.response = response;
	        if (response.length == 0) throw new NotFoundError('track doesn\'t exist');
	    }).then(function() {
            return this.response[0];
        }).asCallback(callback);
    }
};

export default TrackService;