'use strict';

import React from 'react';
import Fluxible from 'fluxible';
import fetchrPlugin from 'fluxible-plugin-fetchr';
import Router from 'fluxible-router';
import AppComponent from './app/components/app.jsx';

var app = new Fluxible({
    component: AppComponent
});

app.plug(fetchrPlugin({
    xhrPath: '/service'
}));

// register routes
var routes = {
	home: require('./home/routes').home,
	track: require('./track/routes').track
};
var RouteStore = Router.RouteStore.withStaticRoutes(routes);

// register stores
app.registerStore(RouteStore);
app.registerStore(require('./track/store'));

export default app;
