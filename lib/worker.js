'use strict';

var babelRegister = require('babel/register');
var Promise = require('bluebird');
var debug = require('debug');

// config logger
debug.enable('*');
debug = debug('tracker:worker');

// add bluebird support
Promise.promisifyAll(require("mongodb"));
Promise.promisifyAll(require("mongodb").MongoClient);
Promise.promisifyAll(require("sendgrid"));

// register queues
var analyseQueue = require('./track/queues/analyseTrack');
var shareQueue = require('./track/queues/shareTrack');

// monitor queues
monitorQueue(analyseQueue);
monitorQueue(shareQueue);

debug('started');

process.on('SIGTERM', function() {
	// stop worker
	debug('shutdown');

	Promise.all([
		analyseQueue.close(),
		shareQueue.close()
	]).then(function() {
		process.exit(0);
	})
});

function monitorQueue(queue) {
	// make sure the queue is clean
	queue.clean(5000, 'completed');

	queue.on('ready', function() {
		debug('queue ' + queue.name + ' ready');
	});

	queue.on('error', function(err) {
	  	debug('queue ' + queue.name + ' error', err);
	})

	queue.on('active', function(job, promise) {
		debug('queue ' + queue.name + ' process job', job.data);
	});

	queue.on('completed', function (job) {
	 	job.remove();
	});

	queue.on('failed', function(job, err) {
		debug('queue ' + queue.name + ' failed to process job', job.data);
		console.error(err.stack);
	});
}