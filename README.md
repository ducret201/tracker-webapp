# Tracker Webapp

## Getting started

```bash
npm install
npm run dev
```

This will use `nodemon` and `webpack` to watch for changes and restart and rebuild as needed.

Open http://localhost:3000
