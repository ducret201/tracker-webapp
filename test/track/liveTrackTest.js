'use strict';

var http = require('http');

var trackId = '2f97c815-64e2-490f-9f0b-65b29c87724c';

var position = {
	"type": "Feature",
	"geometry": {
	    "type": "MultiPoint",
	    "coordinates": [[46.086035, 13.381008]]
	}
};

function sendTrackPosition(trackId, position) {

	var data = JSON.stringify(position);

	var options = {
	  	host: 'localhost',
	  	port: '3000',
	  	path: '/api/v1/track/' + trackId + '/position',
	  	method: 'POST',
	  	headers: {
	    	'Content-Type': 'application/json',
	      	'Content-Length': data.length
	  	}
	};

  	var post = http.request(options);

  	post.write(data);
  	post.end();	
}

function schedule() {
	position.geometry.coordinates[0][1] = position.geometry.coordinates[0][1] + 0.1;

	setTimeout(function() {
		sendTrackPosition(trackId, position);
		schedule();
	}, 2000);
}

schedule();